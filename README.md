# Collatz Conjecture

[![pipeline status](https://gitlab.com/jsiebahn/collatz-conjecture/badges/main/pipeline.svg)](https://gitlab.com/jsiebahn/collatz-conjecture/-/commits/main)
[![coverage report](https://gitlab.com/jsiebahn/collatz-conjecture/badges/main/coverage.svg)](https://gitlab.com/jsiebahn/collatz-conjecture/-/commits/main)

Some Java Code that deals with the
[Collatz conjecture](https://en.wikipedia.org/wiki/Collatz_conjecture).

In short, the Collatz conjecture describes a sequence as follows:

- start with any positive integer
- if the current number is even, the next is half of the current
- if the current number is odd, the next is three times the current plus 1
- repeat with the new number

The assumption is that every sequence built this way ends with the cycle `4`, `2`, `1`.

## Requirements

This project is build with Java 11.

## Limitations

This implementation calculates the sequence using `Long`.
Every sequence that will produce number greater than `Long.MAX_VALUE` will stop with an
`AssertionError`.

## Command Line Usage

The project can be build as a jar with executable main class:

```console
$ ./gradlew clean jar
BUILD SUCCESSFUL in 391ms
3 actionable tasks: 3 executed
```

The produced jar can be used in the command line with java.

Usage:

```
java -jar build/libs/collatz-conjecture.jar [start [known ...]]
```

Examples:

```console
$ java -jar build/libs/collatz-conjecture.jar 8
Starting sequence from: 8
Ended in cycle containing 4
$ java -jar build/libs/collatz-conjecture.jar 16 1 2 4
Starting sequence from: 16
Ended with number 4 of known cycle
$ java -jar build/libs/collatz-conjecture.jar
No start number given, using random: 1625690470235362291
Ended in cycle containing 4
$ java -jar build/libs/collatz-conjecture.jar
No start number given, using random: 1491903063356403303
next after 3356781892551907433 will produce value greater 9223372036854775807
```

Note that bad parameters or a sequence out of long bounds (as in the last example) will return exit
code 1.
