package com.gitlab.jsiebahn.collatz;

import com.gitlab.jsiebahn.collatz.cli.CollatzArgs;

public class Collatz {


  public static void main(String[] args) {
    try {
      ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
      CollatzArgs collatzArgs = new CollatzArgs(args);
      var collatz = new CollatzIterator(
          collatzArgs.getStart(),
          collatzArgs.getStopNumbersEndingInKnownCycles())
          .exhaust();
      if (collatz.inCycle()) {
        System.out.format("Ended in cycle containing %d%n", collatz.getCurrent());
      }
      if (collatz.beginOfKnownCycle()) {
        System.out.format("Ended with number %d of known cycle%n", collatz.getCurrent());
      }
    } catch (AssertionError e) {
      System.err.println(e.getMessage());
      System.exit(1);
    }
  }

}
