package com.gitlab.jsiebahn.collatz;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of the sequence described by the
 * <a href="https://en.wikipedia.org/wiki/Collatz_conjecture">Collatz conjecture</a>. This
 * implementation will exhaust when a cycle is identified or one of the given stop numbers is
 * reached. This implementation uses {@link Long} to manage the current state. When running with
 * assertions enabled, overflows are prevented.
 */
public class CollatzIterator implements Iterator<Long> {

  private final Long start;
  private Long current;

  private final Set<Long> stopNumbers;
  private final Set<Long> seenNumbers = new HashSet<>();

  /**
   * @param start the number from which the sequence starts
   */
  public CollatzIterator(Long start) {
    this(start, null);
  }

  /**
   * @param start the number from which the sequence starts
   * @param stopNumbersEndingInKnownCycles start numbers that end in a known cycle. When one of
   *                                       these numbers is reached, the iterator will exhaust.
   */
  public CollatzIterator(Long start, Set<Long> stopNumbersEndingInKnownCycles) {
    assert Objects.nonNull(start) : "start must not be null";
    assert isPositive(start) : "start must be greater than zero";
    this.start = start;
    this.current = start;
    this.stopNumbers = stopNumbersEndingInKnownCycles != null
        ? stopNumbersEndingInKnownCycles : new HashSet<>();
  }

  @Override
  public boolean hasNext() {
    return !beginOfKnownCycle() && !inCycle();
  }

  @Override
  public Long next() {
    assert isEven(current) || current <= (Long.MAX_VALUE - 1) / 3
        : "next after " + current + " will produce value greater " + Long.MAX_VALUE;
    this.seenNumbers.add(current);
    if (isEven(current)) {
      current = current / 2;
    } else {
      current = current * 3 + 1;
    }
    return current;
  }

  /**
   * Follows the sequence until {@link #inCycle()}, {@link #beginOfKnownCycle()} or an error occurs.
   *
   * @return this iterator exhausted
   */
  public CollatzIterator exhaust() {
    forEachRemaining(l -> {});
    return this;
  }

  public Long getStart() {
    return start;
  }

  public Long getCurrent() {
    return current;
  }

  /**
   * @return if the iterator is in a cycle. If {@code true}, the iterator is exhausted.
   */
  public boolean inCycle() {
    return this.seenNumbers.contains(this.current);
  }

  /**
   * @return if the current number is one of the {@code stopNumbersEndingInKnownCycles}. If
   * {@code true}, the iterator is exhausted.
   */
  public boolean beginOfKnownCycle() {
    return this.stopNumbers.contains(this.current);
  }

  private boolean isPositive(Long number) {
    return number > 0L;
  }

  private boolean isEven(Long number) {
    return number % 2 == 0;
  }
}
