package com.gitlab.jsiebahn.collatz.cli;

import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Set;

public class CollatzArgs {

  private static final SecureRandom SECURE_RANDOM = new SecureRandom();

  private final Long start;
  private final Set<Long> stopNumbersEndingInKnownCycles;

  public CollatzArgs(String[] args) {
    if (args.length == 0) {
      start = nextPositiveRandomLong();
      stopNumbersEndingInKnownCycles = new HashSet<>();
      System.out.format("No start number given, using random: %s%n", start);
    } else {
      start = parseOrFail(args[0]);
      System.out.format("Starting sequence from: %s%n", start);
      stopNumbersEndingInKnownCycles = new HashSet<>();
      for (int i = 1; i < args.length; i++) {
        stopNumbersEndingInKnownCycles.add(parseOrFail(args[i]));
      }
    }
  }

  private long nextPositiveRandomLong() {
    //noinspection OptionalGetWithoutIsPresent
    return SECURE_RANDOM
        .longs(1L, 1L, Long.MAX_VALUE)
        .findFirst()
        .getAsLong();
  }

  private Long parseOrFail(String arg) {
    try {
      return Long.parseLong(arg);
    } catch (NumberFormatException e) {
      throw new AssertionError("Could not parse as number: '" + arg + "'", e);
    }
  }

  public Long getStart() {
    return start;
  }

  public Set<Long> getStopNumbersEndingInKnownCycles() {
    return stopNumbersEndingInKnownCycles;
  }
}
