package com.gitlab.jsiebahn.collatz;

import com.ginsberg.junit.exit.ExpectSystemExitWithStatus;
import com.ginsberg.junit.exit.SystemExitExtension;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SystemExitExtension.class)
class CollatzIT {

  private PrintStream originalOut;
  private PrintStream originalErr;

  private ByteArrayOutputStream out;
  private ByteArrayOutputStream err;

  @BeforeEach
  void mockConsole() {
    originalOut = System.out;
    originalErr = System.err;
    out = new ByteArrayOutputStream();
    System.setOut(new PrintStream(out));
    err = new ByteArrayOutputStream();
    System.setErr(new PrintStream(err));
  }

  @AfterEach
  void resetConsole() {
    System.setOut(originalOut);
    System.setErr(originalErr);
  }

  @Test
  @ExpectSystemExitWithStatus(1)
  void shouldExitForBadArgs() {
    Collatz.main(new String[] {"ABC"});

    var actual = err.toString(UTF_8);

    assertThat(actual)
        .endsWith(String.format("%n"))
        .contains("parse");
  }

  @Test
  void shouldEndInCycle() {
    Collatz.main(new String[] {"8"});

    var actual = out.toString(UTF_8);

    assertThat(actual)
        .endsWith(String.format("%n"))
        .contains("cycle containing 4");
  }

  @Test
  void shouldEndWithKnownNumber() {
    Collatz.main(new String[] {"8", "1", "2", "4"});

    var actual = out.toString(UTF_8);

    assertThat(actual)
        .endsWith(String.format("%n"))
        .contains("with number 4 of known cycle");
  }
}