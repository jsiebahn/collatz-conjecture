package com.gitlab.jsiebahn.collatz;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.LongStream;

import static org.assertj.core.api.Assertions.assertThat;

public class CollatzIteratorIT {

  private static final Logger LOG = LoggerFactory.getLogger(CollatzIteratorIT.class);

  private static final Set<Long> ENDS_IN_KNOWN_CYCLE = new HashSet<>();

  private static final long MIN_INCLUSIVE = 1;
  private static final long MAX_EXCLUSIVE = 100_000;

  @ParameterizedTest
  @MethodSource("createNumbersInTest")
  void shouldCalculate(Long start) {
    var collatz = new CollatzIterator(start, ENDS_IN_KNOWN_CYCLE).exhaust();
    if (collatz.beginOfKnownCycle()) {
      LOG.trace("{} ended in begin of known cycle with start {}", start, collatz.getCurrent());
    }
    if (collatz.inCycle()) {
      LOG.info("{} ended in cycle containing {}", start, collatz.getCurrent());
      assertThat(collatz.getCurrent()).isIn(1L, 2L, 4L);
    }
    ENDS_IN_KNOWN_CYCLE.add(start);
  }

  public static long[] createNumbersInTest() {
    return LongStream.range(MIN_INCLUSIVE, MAX_EXCLUSIVE).toArray();
  }
}
