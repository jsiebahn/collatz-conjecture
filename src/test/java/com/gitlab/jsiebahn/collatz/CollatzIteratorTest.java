package com.gitlab.jsiebahn.collatz;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.*;

class CollatzIteratorTest {

  @Test
  void shouldNotAllowNullAsStart() {
    assertThatExceptionOfType(AssertionError.class).isThrownBy(() -> new CollatzIterator(null));
  }

  @Test
  void shouldNotAllowZeroStart() {
    assertThatExceptionOfType(AssertionError.class).isThrownBy(() -> new CollatzIterator(0L));
  }

  @Test
  void shouldNotAllowNegativeStart() {
    assertThatExceptionOfType(AssertionError.class).isThrownBy(() -> new CollatzIterator(-1L));
  }

  @Test
  void shouldDivideEvenByTwo() {
    CollatzIterator collatzIterator = new CollatzIterator(100L);
    assertThat(collatzIterator).hasNext();
    Long next = collatzIterator.next();
    assertThat(next).isEqualTo(50);
  }

  @Test
  void shouldMultipleAndAddOdd() {
    CollatzIterator collatzIterator = new CollatzIterator(99L);
    assertThat(collatzIterator).hasNext();
    Long next = collatzIterator.next();
    assertThat(next).isEqualTo(298);
  }

  @Test
  void shouldIdentifyCycle() {
    CollatzIterator collatzIterator = new CollatzIterator(8L);

    int iterations = 0;
    while (collatzIterator.hasNext()) {
      iterations++;
      collatzIterator.next();
      assertThat(iterations).isLessThan(99); // fail fast
    }

    assertThat(collatzIterator.getCurrent()).isEqualTo(4);
    assertThat(iterations).isEqualTo(4);
    assertThat(collatzIterator.inCycle()).isTrue();
    assertThat(collatzIterator.beginOfKnownCycle()).isFalse();
  }

  @Test
  void shouldStopImmediatelyWithCustomStopNumbers() {
    CollatzIterator collatzIterator = new CollatzIterator(16L, Set.of(16L));
    assertThat(collatzIterator).isExhausted();
    assertThat(collatzIterator.inCycle()).isFalse();
    assertThat(collatzIterator.beginOfKnownCycle()).isTrue();
  }

  @Test
  void shouldProvideStart() {
    CollatzIterator collatzIterator = new CollatzIterator(123L);
    assertThat(collatzIterator.getStart()).isEqualTo(123);
  }

  @Test
  void shouldStopIfTooLarge() {
    CollatzIterator collatzIterator = new CollatzIterator(3074457345618258603L);
    assertThatExceptionOfType(AssertionError.class).isThrownBy(collatzIterator::next);
  }

  @Test
  void shouldContinueLargeEven() {
    CollatzIterator collatzIterator = new CollatzIterator(3074457345618258608L);
    assertThatCode(collatzIterator::next).doesNotThrowAnyException();
  }

  @Test
  void shouldExhaust() {
    CollatzIterator collatzIterator = new CollatzIterator(16L).exhaust();

    assertThat(collatzIterator).isExhausted();
  }
}