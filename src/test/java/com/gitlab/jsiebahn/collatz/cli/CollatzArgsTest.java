package com.gitlab.jsiebahn.collatz.cli;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

class CollatzArgsTest {

  @Test
  void shouldParseAllArgs() {
    var collatzArgs = new CollatzArgs(new String[]{"123", "1", "2", "4"});

    assertThat(collatzArgs)
        .extracting(CollatzArgs::getStart, CollatzArgs::getStopNumbersEndingInKnownCycles)
        .contains(123L, Set.of(1L, 2L, 4L));
  }

  @Test
  void shouldUseRandomStartWithoutArgs() {
    var collatzArgs = new CollatzArgs(new String[0]);

    assertSoftly(softly -> {
      softly.assertThat(collatzArgs.getStart()).isNotNull();
      softly.assertThat(collatzArgs.getStopNumbersEndingInKnownCycles()).isEqualTo(Set.of());
    });
  }

  @RepeatedTest(1_000)
  void shouldOnlySelectPositiveNumbers() {
    var collatzArgs = new CollatzArgs(new String[0]);

    assertThat(collatzArgs.getStart()).isPositive();
  }

  @RepeatedTest(1_000)
  void shouldSelectRandomNumbers() {
    var collatzArgs1 = new CollatzArgs(new String[0]);
    var collatzArgs2 = new CollatzArgs(new String[0]);

    assertThat(collatzArgs1.getStart()).isNotEqualTo(collatzArgs2.getStart());
  }

  @Test
  void shouldFailIfStartIsNaN() {
    String[] givenArgs = {"ABC"};

    assertThatExceptionOfType(AssertionError.class)
        .isThrownBy(() -> new CollatzArgs(givenArgs))
        .withMessageContaining("parse")
        .withMessageContaining("number")
        .withMessageContaining("ABC");
  }

  @Test
  void shouldFailIfStopValueIsNaN() {
    String[] givenArgs = {"1001", "DEF"};

    assertThatExceptionOfType(AssertionError.class)
        .isThrownBy(() -> new CollatzArgs(givenArgs))
        .withMessageContaining("parse")
        .withMessageContaining("number")
        .withMessageContaining("DEF");
  }
}